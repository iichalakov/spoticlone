var currentPlaylist = [];
var audioElement;
var mousedown = false;
var currentIndex = 0;
var repeat = false;
var shuffle = false;
var shufflePlaylist = [];
var tempPlaylist =[];
var userLoggedIn;
var timer;

$(document).click(function(click){
    var target = $(click.target);
    if(!target.hasClass("item") && !target.hasClass("optionsButton")){
        hideOptionMenu();
    }
});
$(window).scroll(function () {
    hideOptionMenu();
});
$(document).on("change","select.playlist", function(){
    var select = $(this);
    var playlistId = select.val();
    var songId = select.prev(".songId").val();
    $.post("includes/handlers/ajax/addToPlaylist.php",{playlist_id: playlistId, song_id: songId}).done(function(error){
        if(error != ""){
            alert(error);
            return;
        }
        hideOptionMenu();
        select.val("");
    });
});

function Audio(){

    this.currentlyPlaying;
    this.audio = document.createElement('audio');

    this.audio.addEventListener("ended", function(){
        nextSong();
    });

    this.audio.addEventListener("canplay", function(){
        var duration = formatTime(this.duration)
        $(".progressTime.remaining").text(duration);
        // updateVolumeBar(this); //HOW to add it in playing bar
    });

    this.audio.addEventListener("timeupdate", function(){
        if(this.duration){
            updateTimeBar(this);
        }
    });

    this.audio.addEventListener("volumechange", function(){
        if(this.duration){
            updateVolumeBar(this);
        }
    });

    this.setTrack = function(track){
        this.currentlyPlaying = track;
        this.audio.src = track.path;
    }

    this.play = function(){
        this.audio.play();
    }
    this.pause = function(){
        this.audio.pause();
    }
    this.setTime = function(seconds){
        this.audio.currentTime = seconds;
    }

}//end audio function

function formatTime(seconds){
    var time = Math.round(seconds);
    var minutes = Math.floor(time / 60);
    var seconds = time - (minutes*60);
    var extraZero = (seconds < 10) ? "0" : "";
    return minutes + ":"+ extraZero + seconds;
}
function updateTimeBar(audio){
    $(".progressTime.current").text(formatTime(audio.currentTime));
    $(".progressTime.remaining").text(formatTime(audio.duration - audio.currentTime));

    var progress= audio.currentTime / audio.duration * 100;
    $(".playbackBar .progress").css("width",progress + "%");
}
function updateVolumeBar(audio){
    var volume = audio.volume * 100;
    $(".volumeBar .progress").css("width",volume + "%");
}
function openPage(url){
    if(timer != null){
        clearTimeout(timer);
    }
    if(url.indexOf("?") == -1){
        url = url + "?";
    }
    var encodedUrl = encodeURI(url + "&userLoggedIn=" + userLoggedIn);
    $("#mainContent").load(encodedUrl);
    $("body").scrollTop(0);
    history.pushState(null,null,url);
}
function playFirstSong() {
    setTrack(tempPlaylist[0],tempPlaylist,true);
}
function createPlaylist(){
    var popup = prompt("Enter the Name of the playlist");

    if(popup != null){

        $.post("includes/handlers/ajax/createPlaylist.php",{name: popup, username: userLoggedIn}).done(function(error){
            if(error != ""){
                alert(error);
                return;
            }
            openPage("music.php");
        })

    }
}

function deletePlaylist(playlist_id) {
    var prompt = confirm("Are you sure you want to delete this playlist");

    if(prompt){
        $.post("includes/handlers/ajax/deletePlaylist.php",{playlist_id: playlist_id}).done(function(error){
            if(error != ""){
                alert(error);
                return;
            }
            openPage("music.php");
        })
    }
}

function showOptionMenu(button) {
    var songId = $(button).prevAll(".songId").val();
    var menu = $(".optionMenu");
    var menuWidth = menu.width();
    menu.find(".songId").val(songId);
    var scrollTop = $(window).scrollTop();
    var elementOffset = $(button).offset().top;
    var top = elementOffset - scrollTop;
    var left = $(button).position().left;

    menu.css({"top": top + "px","left": left - menuWidth + "px", "display": "inline"});
}
function hideOptionMenu() {
    var menu = $(".optionMenu");
    if(menu.css("display") != "none"){
        menu.css("display","none");
    }
}

function removeFromPlaylist(button,playlist_id) {
    var songId = $(button).prevAll(".songId").val();

    $.post("includes/handlers/ajax/removeFromPlaylist.php",{playlist_id: playlist_id, song_id: songId}).done(function(error){
        if(error != ""){
            alert(error);
            return;
        }
        openPage("playlist.php?id=" + playlist_id);
    })
}
function logout() {
    $.post("includes/handlers/ajax/logout.php", function () {
        location.reload();
    });
}

function updateEmail(emailClass) {
    var emailValue = $("." + emailClass).val();

    $.post("includes/handlers/ajax/updateEmail.php",{email: emailValue, username: userLoggedIn}).done(function(response){
        $("." + emailClass).nextAll(".message").text(response);
    });
}
function updatePassword(oldPasswordClass,newPasswordClass,confirmPasswordClass) {
    var oldPassword = $("." + oldPasswordClass).val();
    var newPassword = $("." + newPasswordClass).val();
    var confirmPassword = $("." + confirmPasswordClass).val();

    $.post("includes/handlers/ajax/updatePassword.php",{oldPassword: oldPassword, newPassword: newPassword,confirmPassword: confirmPassword, username: userLoggedIn }).done(function(response){
        $("." + oldPasswordClass).nextAll(".message").text(response);
    });
}