<?php
    include("includes/config/config.php");
    include("includes/classes/Artist.php");
    include("includes/classes/Album.php");
    include("includes/classes/Song.php");
    include("includes/classes/User.php");
    include("includes/classes/Playlist.php");

   if(isset($_SESSION['userLoggedIn'])){
        $userLoggedIn = new User($conn, $_SESSION['userLoggedIn']);
        $username = $userLoggedIn->getUsername();
        echo "<script>userLoggedIn = '$username'; </script>";

    }else {
        header("Location: login.php");
    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/script.js"></script>
</head>
<body>
    <div class="mainContainer">
        <div class="topContainer">
            <?php include("includes/navBar.php")?>
            <div class="mainViewContainer">
                <div class="mainContent" id="mainContent">