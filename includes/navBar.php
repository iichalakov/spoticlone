<div class="navigationBar">
    <nav class="navBar">
        <span class="logo" role="link" tabindex="0" onClick="openPage('index.php')">
            <img src="img/bar-icons/logo.png" alt="">
        </span>

        <div class="group">
            <div class="navItem">
            <span role='link' tabindex='0' onClick="openPage('search.php')" class="navItemLink" >Search
                    <img src="img/bar-icons/search.png" alt="Search" class="icon">
            </span>

            </div>
        </div>
        <div class="group">
            <div class="navItem">
                <span role="link" tabindex="0" onClick="openPage('browse.php')"class="navItemLink">Browse</span>
            </div>
            <div class="navItem">
                <span role="link" tabindex="0" onClick="openPage('music.php')" class="navItemLink">Your Music</span>
            </div>
            <div class="navItem">
                <span role="link" tabindex="0" onClick="openPage('settings.php')" class="navItemLink"><?php echo $userLoggedIn->getName(); ?></span>
            </div>
        </div>
    </nav>
</div>