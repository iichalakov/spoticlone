<?php
    class PlaylistValidate{

        private $errorArray;
        private  $conn;
        public function __construct($conn){
            $this->conn = $conn;
            $this->errorArray = array();
        }

        public function checkPlaylistName($name){
            $checkPlaylistNameQuery = mysqli_query($this->conn, "SELECT name FROM playlist WHERE name='$name' ");
            if(mysqli_num_rows($checkPlaylistNameQuery)!=0){
                array_push($this->errorArray, Constants::$playlistNameTaken);
            }
        }

    }//end class
?>