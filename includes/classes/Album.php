<?php
    class Album{

        private $id;
        private $conn;
        private $title;
        private $artist_id;
        private $genre_id;
        private $albumPicturePath;

        public function __construct($conn, $id){
            $this->conn = $conn;
            $this->id = $id;

            $sql = "SELECT * FROM album WHERE id='$this->id'";
            $albumQuery = $this->conn->prepare($sql);
            $albumQuery->execute();
            $res = $albumQuery->get_result();
            $album = $res->fetch_assoc();

            $this->title = $album['title'];
            $this->artist_id = $album['artist_id'];
            $this->genre_id = $album['genre_id'];
            $this->albumPicturePath = $album['albumPicturePath'];
        }

        public function getAlbumTitle(){
            return $this->title;
        }
        public function getArtist(){
            return new Artist($this->conn, $this->artist_id);
        }
        public function getAlbumPicturePath(){
            return $this->albumPicturePath;
        }
        public function getGenre(){
            return $this->genre_id;
        }
        public function getNumberSongs(){
            $sql = "SELECT id FROM song WHERE album='$this->id'";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->get_result();
            return mysqli_num_rows($res);
            mysqli_close($conn);
        }

        public function getSongsId(){
            $songArray = array();
            $sql = "SELECT id FROM song WHERE album='$this->id' ORDER BY albumOrder ASC";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->get_result();
            while($row = mysqli_fetch_array($res)){
                array_push($songArray,$row['id']);
            }
            return $songArray;
            // mysqli_close($conn);
        }



    }//end class
?>