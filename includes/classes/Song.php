<?php
    class Song{

        private $id;
        private $conn;
        private $data;
        private $title;
        private $artist_id;
        private $album_id;
        private $genre_id;
        private $duration;
        private $songPath;


        public function __construct($conn, $id){
            $this->conn = $conn;
            $this->id = $id;

            $sql = "SELECT * FROM song WHERE id='$this->id'";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->get_result();
            $this->data = $res->fetch_assoc();

            $this->title = $this->data['title'];
            $this->artist_id = $this->data['artist'];
            $this->album_id = $this->data['album'];
            $this->genre_id = $this->data['genre'];
            $this->duration = $this->data['duration'];
            $this->songPath = $this->data['path'];
        }

        public function getTitle(){
            return $this->title;
        }
        public function getId(){
            return $this->id;
        }
        public function getArtist(){
            return new Artist($this->conn, $this->artist_id);
        }
        public function getAlbum(){
            return new Album($this->conn, $this->album_id);
        }
        public function getSongPath(){
            return $this->songPath;
        }
        public function getDuration(){
            return $this->duration;
        }
        public function getData(){
            return $this->data;
        }
        public function getGenre(){
            return $this->genre_id;
        }

    }//end class
?>