<?php
    class Playlist{

        private $id;
        private $conn;
        private $name;
        private $owner;

        public function __construct($conn, $data){

            if(!is_array($data)){
                $sql = mysqli_query($conn, "SELECT * FROM playlist WHERE id='$data'");
                $data = mysqli_fetch_array($sql);
            }

            $this->conn = $conn;
            $this->id = $data['id'];
            $this->name = $data['name'];
            $this->owner = $data['owner'];
        }

        public function getPlaylistName()
        {
           return $this->name;
        }
        public function getPlaylistOwner()
        {
           return $this->owner;
        }
        public function getPlaylistId()
        {
           return $this->id;
        }
        public function getNumberSongs(){
            $sql = "SELECT song_id FROM playlistsong WHERE playlist_id ='$this->id'";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->get_result();
            return mysqli_num_rows($res);

        }
        public function getSongsId(){
            $songArray = array();
            $sql = "SELECT song_id FROM playlistsong WHERE playlist_id='$this->id' ORDER BY playlistOrder ASC";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->get_result();
            while($row = mysqli_fetch_array($res)){
                array_push($songArray,$row['song_id']);
            }
            return $songArray;
            // mysqli_close($conn);
        }
        public static function getPlaylistDropdown($conn,$username)
        {
            $dropdown = '<select  class="item playlist">
            <option value="">Add to playlist</option>';

            $state = "SELECT id, name FROM playlist WHERE owner='$username'";
            $query = mysqli_query($conn, $state);

            while($row = mysqli_fetch_array($query)){
                $id = $row['id'];
                $name = $row['name'];
                $dropdown = $dropdown . "<option value='$id'>$name</option>";
            }

        return $dropdown . "</select>";
        }


    }//end class
?>