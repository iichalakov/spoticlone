<?php
    class Artist{

        private $id;
        private $conn;

        public function __construct($conn, $id){
            $this->conn = $conn;
            $this->id = $id;
        }

        public function getName(){
            //working non-prepare
            // $sql = "SELECT name FROM artist WHERE id='$this->id'";
            // $artistQuery = mysqli_query($this->conn,$sql);
            // $artist = mysqli_fetch_array($artistQuery);
            // return $artist['name'];

            //working with prepare below
            $sql = "SELECT name FROM artist WHERE id='$this->id'";
            // print_r($sql);
            $stmt = $this->conn -> prepare($sql);
            $stmt->execute();
            $res = $stmt->get_result();
            $asd = $res->fetch_assoc();
            // print_r($asd);
            return $asd['name'];
            // print_r($artist['name']);
            // mysqli_close($conn);
        }

        public function getSongIds(){
            $songArray = array();
            $sql = "SELECT id FROM song WHERE artist='$this->id' ORDER BY plays DESC";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->get_result();
            while($row = mysqli_fetch_array($res)){
                array_push($songArray,$row['id']);
            }
            return $songArray;
            mysqli_close($conn);
        }
        public function getId(){
            return $this->id;
        }
    }//end class
?>