<?php
    class AccountValidate{

        private $errorArray;
        private  $conn;
        public function __construct($conn){
            $this->conn = $conn;
            $this->errorArray = array();
        }

        public function login($un,$ps){
            $ps = md5($ps);
            $query = mysqli_query($this->conn," SELECT * FROM user WHERE username='$un' AND password='$ps'");
            if(mysqli_num_rows($query) ==1){
                return true;
            }else {
                array_push($this->errorArray,Constants::$loginFailed);
                return false;
            }
        }

        public function register($un, $em, $ps, $cps){
          $this->validateUsername($un);
          $this->validateEmail($em);
          $this->validatePasswrods($ps, $cps);

          if(empty($this->errorArray) == true){
              //insert into db
              return $this->insertUserDetails($un,$em,$ps);
          }else {
              return false;
          }

        }
        private function insertUserDetails($un,$em,$ps){
            $encryptedPS = md5($ps);
            $profilePicture = "img/profile-pictures/head.png"; // need to find profile picture
            $date = date("Y-m-d");
           // $result = mysqli_query($this->conn,"INSERT INTO user VALUES('','$un','$encryptedPS','$em','$date','$profilePicture')");
           // return $result;
            //ABOVE WORKS same as BELOW

            $this->conn = new PDO('mysql:host=localhost;dbname=spoticlone','root','');
            $sql = "INSERT INTO user VALUES ('','$un','$encryptedPS','$em','$date','$profilePicture')";
            $prepStatement = $this->conn->prepare($sql);
            $result = $prepStatement->execute();
            return $result;
            $prepStatement->close();
            $conn->close();
        }

        private function validateUsername($un){
            if(strlen($un) > 25 || strlen($un) < 5){
                array_push($this->errorArray, Constants::$usernameCharacters);
                return;
            }

            //check for existing username
            $checkUsernameQuery = mysqli_query($this->conn, "SELECT username FROM user WHERE username='$un' ");
            if(mysqli_num_rows($checkUsernameQuery)!=0){
                array_push($this->errorArray, Constants::$usernameTaken);
            }

        }

        private function validateEmail($em){
            if(!filter_var($em, FILTER_VALIDATE_EMAIL)){
                array_push($this->errorArray, Constants::$emailInvalid);
                return;
            }
            $checkEmailQuery = mysqli_query($this->conn, "SELECT username FROM user WHERE email='$em' ");
            if(mysqli_num_rows($checkEmailQuery)!=0){
                array_push($this->errorArray, Constants::$emailTaken);
            }
            //
        }
        private function validatePasswrods($ps , $cps){
            if($ps != $cps){
                array_push($this->errorArray,  Constants::$passwordsNotmatch);
                return;
            }
            if(preg_match('/[^A-Za-z0-9]/',$ps)){
                array_push($this->errorArray,  Constants::$passwrodsNotAlphanumeric);
                return;
            }

        }

        public function getError($error){
            if(!in_array($error, $this->errorArray)){
                $error = "";
            }
            return "<span class='errorMessage'>$error</span>";
        }

        public function checkPlaylistName($name){
            $checkPlaylistNameQuery = mysqli_query($this->conn, "SELECT name FROM playlist WHERE name='$name' ");
            if(mysqli_num_rows($checkPlaylistNameQuery)!=0){
                array_push($this->errorArray, Constants::$playlistNameTaken);
            }
        }

    }//end class
?>