<?php
    class Constants{

        public static $usernameCharacters = "Your username must be between 5 and 25 characters";
        public static $emailInvalid = "Your EMAIL is unvalid";
        public static $passwordsNotmatch = "Your password must match";
        public static $passwrodsNotAlphanumeric ="Your password can contain ONLY numbers and letters";
        public static $usernameTaken = "Your username is taken";
        public static $emailTaken = "Your email is already in use";
        public static $loginFailed = "Invalid username or password";
        public static $playlistNameTaken = "Playlist name is ALREADY taken";
    }

?>