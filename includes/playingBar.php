<?php

    $songArray = array();
    $sql = "SELECT id FROM song ORDER BY RAND() LIMIT 3";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $res = $stmt->get_result();
    while($row = mysqli_fetch_array($res)){
        array_push($songArray,$row['id']);
    }
    $jsonArray = json_encode($songArray);
    // mysqli_close($conn);

?>

<script>

    $(document).ready(function(){
        var newPlaylist = <?php echo $jsonArray; ?>;
        audioElement = new Audio();
        setTrack(newPlaylist[0],newPlaylist,false);
        updateVolumeBar(audioElement.audio);// FIXED

        $("#nowPlayingBarsContainer").on("mousedown touchstart mousemove touchmove", function(event){
            event.preventDefault();
        });

        $(".playbackBar .progressBar").mousedown(function(){
            mousedown = true;
        });
        $(".playbackBar .progressBar").mousemove(function(event){
            if(mousedown){
                timeFromoffSet(event,this);
            }
        });
        $(".playbackBar .progressBar").mouseup(function(event){
            timeFromoffSet(event,this);
        });
        //volume below
        $(".volumeBar .progressBar").mousedown(function(){
            mousedown = true;
        });
        $(".volumeBar .progressBar").mousemove(function(event){
            if(mousedown){
                var percent = event.offsetX / $(this).width();

                if(percent >= 0 && percent <=1){
                    audioElement.audio.volume = percent;
                }
            }
        });
        $(".volumeBar .progressBar").mouseup(function(event){
            var percent = event.offsetX / $(this).width();

            if(percent >= 0 && percent <=1){
                audioElement.audio.volume = percent;
            }
        });

        $(document).mouseup(function(){
            mousedown = false;
        });

    });

    function setTrack(trackId,newPlaylist,play){

        if(newPlaylist != currentPlaylist){
            currentPlaylist = newPlaylist;
            shufflePlaylist = newPlaylist.slice();
            // shuffleArray(shufflePlaylist); for shuffle
        }

        if(shuffle == true){
            currentIndex = shufflePlaylist.indexOf(trackId);
        }else {
            currentIndex = currentPlaylist.indexOf(trackId);
        }
        pauseSong();

        $.post("includes/handlers/ajax/getSongJson.php" , {songId: trackId}, function(data){

            var track = JSON.parse(data);
            console.log(currentPlaylist);
            $(".trackName span").text(track.title);
            // console.log(track);

            $.post("includes/handlers/ajax/getArtistJson.php" , {artist_id: track.artist}, function(data){
                var artist = JSON.parse(data);
                // console.log(artist);
                $(".trackArtist span").text(artist.name);
                $(".trackArtist span").attr("onClick","openPage('artist.php?id= " + artist.id +" ')");
            });
            $.post("includes/handlers/ajax/getAlbumJson.php" , {album_id: track.album}, function(data){
                var album = JSON.parse(data);
                // console.log(album);
                $(".albumLink img").attr("src",album.albumPicturePath);
                $(".albumLink img").attr("onClick","openPage('album.php?id= " + album.id +" ')");
                $(".trackName span").attr("onClick","openPage('album.php?id= " + album.id +" ')");
            });
            audioElement.setTrack(track);

            if(play){
                playSong();
            }
        });
    }//end setTrack

    function playSong(){
        if(audioElement.audio.currentTime == 0){
            $.post("includes/handlers/ajax/updatePlays.php" , {songId: audioElement.currentlyPlaying.id});
        }
        $(".controlButton.play").hide();
        $(".controlButton.pause").show();
        audioElement.play();
    }
    function pauseSong(){
        $(".controlButton.play").show();
        $(".controlButton.pause").hide();
        audioElement.pause();
    }
    function timeFromoffSet(mouse, progressBar){
        var percent = mouse.offsetX / $(progressBar).width() * 100;
        var seconds = audioElement.audio.duration * (percent / 100);
        audioElement.setTime(seconds);
    }
    function nextSong(){
        console.log(currentIndex);
        if(repeat == true){
            audioElement.setTime(0);
            playSong();
            return;
        }
        if(currentIndex == currentPlaylist.lenght-1){
            currentIndex = 0;
            console.log(currentIndex);

        }
        else {
            currentIndex++;
        }

            // console.log(currentIndex);
        console.log(currentIndex);
        // var trackToPlay = shuffle ? shufflePlaylist[currentIndex] : currentPlaylist[currentIndex]; for shuffle playlist
        var trackToPlay = currentPlaylist[currentIndex];
        setTrack(trackToPlay,currentPlaylist,true);
    }
    function setRepeat(){
        repeat =  !repeat;
        var imageName = repeat ? "repeat-active.png" : "repeat.png";
        $(".controlButton.repeat img").attr("src","img/bar-icons/" + imageName);
    }
    function previousSong(){
        if (audioElement.audio.currentTime >= 3 || currentIndex == 0) {
            audioElement.setTime(0);
        }else {
            currentIndex--;
            setTrack(currentPlaylist[currentIndex], currentPlaylist, true);
            console.log(currentIndex);
        }
    }
    function setMute() {
        audioElement.audio.muted = !audioElement.audio.muted;
        var imageName = audioElement.audio.muted ? "volume-mute.png" : "volume.png";
        $(".controlButton.volume img").attr("src","img/bar-icons/" + imageName);
    }
    function setShuffle() {
        shuffle = !shuffle;
        var imageName =shuffle ? "shuffle-active.png" : "shuffle.png";
        $(".controlButton.shuffle img").attr("src","img/bar-icons/" + imageName);
        console.log(currentPlaylist);
        console.log(shufflePlaylist);

        if (shuffle == true) {
            shuffleArray(shufflePlaylist);
            currentIndex = shufflePlaylist.indexOf(audioElement.currentlyPlaying.id);
        }else {
            currentIndex = currentPlaylist.indexOf(audioElement.currentlyPlaying.id);
        }

    }
    function shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
        }
    }

</script>


<div class="nowPlayingBars" id="nowPlayingBarsContainer">
    <div class="nowPlayingLeft">
        <div class="content">
            <span class="albumLink">
                <img role="link" tabindex="0" src="" class="albumArtwork">
            </span>
            <div class="trackInfo">
                <span class="trackName">
                    <span role="link" tabindex="0"></span>
                </span>
                <span class="trackArtist">
                    <span role="link" tabindex="0"></span>
                </span>
            </div>
        </div>
    </div>

    <div class="nowPlayingCenter">
        <div class="content playerControls">
            <div class=" buttons">
                <button class="controlButton shuffle" title="Shuffle button" onClick="setShuffle()">
                    <img src="img/bar-icons/shuffle.png">
                </button>
                <button class="controlButton previous" title="Previous button" onClick="previousSong()">
                    <img src="img/bar-icons/previous.png">
                </button>
                <button class="controlButton play" title="Play button" onClick="playSong()">
                    <img src="img/bar-icons/play.png">
                </button>
                <button class="controlButton pause" title="Pause button" style="display:none" onClick="pauseSong()">
                    <img src="img/bar-icons/pause.png">
                </button>
                <button class="controlButton next" title="Next button" onClick="nextSong()">
                    <img src="img/bar-icons/next.png">
                </button>
                <button class="controlButton repeat" title="Repeat button" onClick="setRepeat()">
                    <img src="img/bar-icons/repeat.png">
                </button>
            </div>

            <div class="playbackBar">
                <span class="progressTime current">0.00 </span>
                <div class="progressBar">
                    <div class="progressBarBackground">
                        <div class="progress"></div>
                    </div>
                </div>
                <span class="progressTime remaining">0.00 </span>
            </div>

        </div>
    </div>

    <div class="nowPlayingRight">
        <div class="volumeBar">
        <button class="controlButton volume" title="Volume" onClick="setMute()">
                <img src="img/bar-icons/volume.png" alt="Volume">
        </button>
            <div class="progressBar">
                    <div class="progressBarBackground">
                        <div class="progress"></div>
                    </div>
            </div>
        </div>
    </div>
</div>