<?php include("includes/handlers/includedFiles.php");
    if(isset($_GET['id'])){
        $playlist_id = $_GET['id'];
    }
    else {
        header("Location:index.php");
    }

    $playlist = new Playlist($conn, $playlist_id);
    $owner = new User($conn,$playlist->getPlaylistOwner());

?>

<div class="entityInfo">
    <div class="leftSection">
        <div class="playlistImage">
            <img src="img/bar-icons/playlist.png">
        </div>
    </div>
    <div class="rightSection">
        <h2> <?php echo $playlist->getPlaylistName();?></h2>
        <p> By  <?php echo $playlist->getPlaylistOwner();?></p>
        <p><?php echo $playlist->getNumberSongs();?> songs </p>
        <button class="button" onClick="deletePlaylist('<?php echo $playlist_id?>')">Delete Playlist</button>
    </div>
</div>

<div class="tracklistContainer">
    <ul class="tracklist">
        <?php
            $songIdArray = $playlist->getSongsId();
            $i = 1;
            foreach($songIdArray as $songId){
                $playlistSong = new Song($conn,$songId);
                $songArtist = $playlistSong->getArtist();

                echo "<li class='tracklistRow'>
                        <div class='trackCount'>
                            <img class='play' src='img/bar-icons/play-white.png' onClick='setTrack(" . $playlistSong->getId() . ",tempPlaylist,true)'>
                            <span class='trackNumber'>$i</span>
                        </div>

                        <div class='trackInfo'>
                            <span class='trackName'>" . $playlistSong->getTitle() ." </span>
                            <span class='artistName'>" . $songArtist->getName() ." </span>
                        </div>

                        <div class='trackOptions'>
                            <input type='hidden' class='songId' value='" . $playlistSong->getId() .  "'>
                            <img class='optionsButton' src='img/bar-icons/more.png' onClick='showOptionMenu(this)'>
                        </div>
                        <div class='trackDuration'>
                            <span class='duration'>" . $playlistSong->getDuration() . "</span>
                        </div>
                    </li>";
                $i++;
            }
        ?>
        <script>
            var tempSongIds = '<?php echo json_encode($songIdArray); ?>';
            tempPlaylist = JSON.parse(tempSongIds);
            // console.log(tempPlaylist);
        </script>
    </ul>

</div>
<nav class="optionMenu">
            <input type="hidden" class="songId">
            <?php echo Playlist::getPlaylistDropdown($conn, $userLoggedIn->getUsername());?>
            <div class="item" onClick="removeFromPlaylist(this, '<?php echo $playlist_id;?>')">Remove from Playlist</div>
</nav>

