<?php
    include("includes/config/config.php");
    include("includes/classes/accountValidate.php");
    include("includes/classes/Constants.php");
    $accountVal = new AccountValidate($conn);
    include("includes/handlers/loginHandler.php");
?>
<link href="styles/style.css" rel="stylesheet">
<link href="styles/background.css" rel="stylesheet">


<form class="box" method="POST">
        <h1>Login</h1>
        <input type="text" name="username" placeholder="Username">
        <input type="password" name="password" placeholder="Password">

        <?php echo $accountVal->getError(Constants::$loginFailed);?>
        <input type="submit" value="Login" id="login" name="login">
        <a href="register.php">You dont have an account? </a>
</form>


