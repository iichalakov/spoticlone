<?php
    include("includes/handlers/includedFiles.php");

    if(isset($_GET['term'])) {
        $term = urldecode($_GET['term']);
    }
    else {
        $term = "";
    }
?>

<div class="searchContainer">
    <h4>Search for Artist,Song or Album</h4>
    <input type="text" class="searchInput" value="<?php echo $term?>" placeholder="Start typing..." onfocus="this.value = this.value">
</div>

<script>
 $(".searchInput").focus();

    $(function(){
        $(".searchInput").keyup(function(){
            clearTimeout(timer);

            timer = setTimeout(() => {
                var val = $(".searchInput").val();
                openPage("search.php?term="+val);
            }, 2000);
        })
    })

</script>

<?php
    if($term == ""){
        exit();
    }
?>

<div class="tracklistContainer borderBottom">
    <h2>Songs</h2>
    <ul class="tracklist">
        <?php
            $songQuery = "SELECT id FROM song WHERE title LIKE '$term%'";
            $stmt = $conn->prepare($songQuery);
            $stmt->execute();
            $res = $stmt->get_result();
            if(mysqli_num_rows($res) == 0){
                echo "<span class='noResult'> No match found" . " -> " . $term . "</span>";
            }

            $songIdArray = array();
            $i = 1;
            while($row = mysqli_fetch_array($res)){

                if($i > 15){
                    break;
                }
                array_push($songIdArray,$row['id']);

                $albumSong = new Song($conn,$row['id']);
                $albumArtist = $albumSong->getArtist();

                echo "<li class='tracklistRow'>
                        <div class='trackCount'>
                            <img class='play' src='img/bar-icons/play-white.png' onClick='setTrack(" . $albumSong->getId() . ",tempPlaylist,true)'>
                            <span class='trackNumber'>$i</span>
                        </div>

                        <div class='trackInfo'>
                            <span class='trackName'>" . $albumSong->getTitle() ." </span>
                            <span class='artistName'>" . $albumArtist->getName() ." </span>
                        </div>

                        <div class='trackOptions'>
                            <input type='hidden' class='songId' value='" . $albumSong->getId() .  "'>
                            <img class='optionsButton' src='img/bar-icons/more.png' onClick='showOptionMenu(this)'>
                        </div>
                        <div class='trackDuration'>
                            <span class='duration'>" . $albumSong->getDuration() . "</span>
                        </div>
                    </li>";
                $i++;
            }
        ?>
        <script>
            var tempSongIds = '<?php echo json_encode($songIdArray); ?>';
            tempPlaylist = JSON.parse(tempSongIds);
            // console.log(tempPlaylist);
        </script>
    </ul>
</div>

<div class="artistContainer borderBottom">
        <h2>Artist</h2>
        <?php
            $artistQuery = "SELECT id FROM artist WHERE name LIKE '$term%'";
            $stmt = $conn->prepare($artistQuery);
            $stmt->execute();
            $res = $stmt->get_result();

            if(mysqli_num_rows($res) == 0){
                echo "<span class=noResult> No match found" . " -> " . $term . "</span>";
            }
            while($row = mysqli_fetch_array($res)){
                $artistFound = new Artist($conn,$row['id']);

                echo " <div class='searchResultRow'>
                            <div class='artistName'>
                                <span role='link' tabindex='0' onClick='openPage(\"artist.php?id=" . $artistFound->getId() ."\")'>
                                    "
                                    . $artistFound->getName();
                                    "
                                </span>
                            </div>
                        </div>
                ";
            }
        ?>
</div>

<div class="albumContainer borderBottom">
    <h2>Albums</h2>
        <?php
            $albumQuery = "SELECT * FROM album WHERE title LIKE '$term%'";
            $stmt = $conn->prepare($albumQuery);
            $stmt->execute();
            $res = $stmt->get_result();

            if(mysqli_num_rows($res) == 0){
                echo "<span class=noResult> No match found" . $term . "</span>";
            }
            while($row = mysqli_fetch_array($res)){

                echo "<div class='gridViewItem'>
                            <span role='link' tabindex='0' onClick='openPage(\"album.php?id=" . $row['id'] . "\")' >
                                <img src='" . $row['albumPicturePath'] . "'>

                                <div class='gridViewInfo'>" . $row['title'] . "</div>
                            </span>
                    </div>";
            }
        ?>
</div>
<nav class="optionMenu">
            <input type="hidden" class="songId">
            <?php echo Playlist::getPlaylistDropdown($conn, $userLoggedIn->getUsername());?>
</nav>

