<?php
  include("includes/handlers/includedFiles.php");
?>

<div class="playlistContainer">
    <div class="gridViewContainer">
        <h2>Playlist</h2>

        <div class="buttonItems">
            <button class="button green" onclick="createPlaylist()">New Playlist</button>
        </div>

        <?php
            $username = $userLoggedIn->getUsername();

            $playlistQuery = "SELECT * FROM playlist WHERE owner= '$username'";
            $stmt = $conn->prepare($playlistQuery);
            $stmt->execute();
            $res = $stmt->get_result();

            if(mysqli_num_rows($res) == 0){
                echo "<span class=noResult> No playlist found.</span>";
            }
            while($row = mysqli_fetch_array($res)){
                $playlist = new Playlist($conn,$row);

                echo "<div class='gridViewItem' role='link' tabindex='0' onclick='openPage(\"playlist.php?id=" . $playlist->getPlaylistId() ."\")'>
                         <div class='playlistImage'>
                            <img src='img/bar-icons/playlist.png'>
                         </div>
                         <div class='gridViewInfo'>" . $row['name'] . "</div>
                    </div>";
            }
        ?>

    </div>
</div>