<?php
    include("includes/config/config.php");
    include("includes/classes/accountValidate.php");
    include("includes/classes/Constants.php");
    $accountVal = new AccountValidate($conn);
    include("includes/handlers/registerHandler.php");

    function getInputValue($nameField){
        if(isset($_POST[$nameField])){
            echo $_POST[$nameField];
        }
    }

?>

<link href = "styles/background.css" rel="stylesheet">
<link href = "styles/registerForm.css" rel="stylesheet">
    <form class="registerForm" method = "POST">

        <label class="labels" for="username">Username</label>
            <div class="esentials">
                <?php echo $accountVal->getError(Constants::$usernameCharacters);?>
                <?php echo $accountVal->getError(Constants::$usernameTaken);?>
                        <!-- <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span> -->
                        <!-- <input type="text" name="loginUsername" id="loginUsername" placeholder="Enter your Username" /> -->
                <input type="text" name="username" placeholder="Enter your Username" value="<?php getInputValue('username')?>" required>
            </div>
        <label class="labels" for="password">Password</label>
            <div class="esentials">
            <?php echo $accountVal->getError(Constants::$passwordsNotmatch);?>
            <?php echo $accountVal->getError(Constants::$passwrodsNotAlphanumeric);?>
                        <!-- <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span> -->
                <input type="password"  name="password" id="password" placeholder="Enter your Password"  required />
            </div>
        <label class="labels" for="password2">Confirm Password</label>
            <div class="esentials">
                        <!-- <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span> -->
                <input type="password" name="confirmPassword" id="confirmPassword" placeholder="Enter your Password" required />
            </div>
        <label class="labels" for="email">Email</label>
            <div class="esentials">
            <?php echo $accountVal->getError(Constants::$emailInvalid);?>
            <?php echo $accountVal->getError(Constants::$emailTaken);?>
                        <!-- <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span> -->
                <input type="email" name="email" id="email" placeholder="Enter your email" value="<?php getInputValue('email')?>" required />
            </div>

            <div class="register">
               <!-- <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span> -->
                <input type="submit" name="register" value="Register" />
            </div>
    </form>
