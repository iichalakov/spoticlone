<?php
    include("includes/handlers/includedFiles.php");
    if(isset($_GET['id'])){
        $artistId = $_GET['id'];
    }
    else {
        header("Location:index.php");
    }
$artist = new Artist($conn, $artistId);
?>

<div class="entityInfo borderBottom">
    <div class="centerSection">
        <div class="artistInfo">
            <h1 class="artistName"><?php echo $artist->getName();?></h1>
            <div class="headerButtons">
                <button class="button green" onClick="playFirstSong()">
                    Play
                </button>
            </div>
        </div>
    </div>
</div>

<div class="tracklistContainer borderBottom">
    <h2>Songs</h2>
    <ul class="tracklist">
        <?php
            $songIdArray = $artist->getSongIds();
            $i = 1;
            foreach($songIdArray as $songId){

                if($i > 10){
                    break;
                }

                $albumSong = new Song($conn,$songId);
                $albumArtist = $albumSong->getArtist();

                echo "<li class='tracklistRow'>
                        <div class='trackCount'>
                            <img class='play' src='img/bar-icons/play-white.png' onClick='setTrack(" . $albumSong->getId() . ",tempPlaylist,true)'>
                            <span class='trackNumber'>$i</span>
                        </div>

                        <div class='trackInfo'>
                            <span class='trackName'>" . $albumSong->getTitle() ." </span>
                            <span class='artistName'>" . $albumArtist->getName() ." </span>
                        </div>

                        <div class='trackOptions'>
                            <input type='hidden' class='songId' value='" . $albumSong->getId() .  "'>
                            <img class='optionsButton' src='img/bar-icons/more.png' onClick='showOptionMenu(this)'>
                        </div>
                        <div class='trackDuration'>
                            <span class='duration'>" . $albumSong->getDuration() . "</span>
                        </div>
                    </li>";
                $i++;
            }
        ?>
        <script>
            var tempSongIds = '<?php echo json_encode($songIdArray); ?>';
            tempPlaylist = JSON.parse(tempSongIds);
            // console.log(tempPlaylist);
        </script>
    </ul>
</div>
<div class="gridViewContainer">
    <h2>Albums</h2>
        <?php
                /*NEED TO TRY WITH PREPARE STATEMENT */
            $albumQuery = mysqli_query($conn,"SELECT * FROM album WHERE artist_id='$artistId'");
            while($row = mysqli_fetch_array($albumQuery)){

                echo "<div class='gridViewItem'>
                            <span role='link' tabindex='0' onClick='openPage(\"album.php?id=" . $row['id'] . "\")' >
                                <img src='" . $row['albumPicturePath'] . "'>

                                <div class='gridViewInfo'>" . $row['title'] . "</div>
                            </span>
                    </div>";
            }
        ?>
</div>
<nav class="optionMenu">
            <input type="hidden" class="songId">
            <?php echo Playlist::getPlaylistDropdown($conn, $userLoggedIn->getUsername());?>
</nav>