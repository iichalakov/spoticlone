<?php
  include("includes/handlers/includedFiles.php");
?>

<div class="userDetails">
    <div class="container borderBottom">
        <h2>Email</h2>
        <input type="text" class="email" name="email" placeholder="Email" value="<?php echo $userLoggedIn->getEmail();?>">
        <span class="message"></span>
        <button class="button" onClick="updateEmail('email')">Save</button>
    </div>

    <div class="container">
        <h2>Password</h2>
        <input type="password" class="oldPassword" name="oldPassword" placeholder="Current password">
        <input type="password" class="newPassword" name="newPassword" placeholder="New password">
        <input type="password" class="confirmPassword" name="confirmPassword" placeholder="Confirm password">
        <span class="message"></span>
        <button class="button" onClick="updatePassword('oldPassword','newPassword','confirmPassword')">Update Password</button>
    </div>

</div>
