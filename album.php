<?php include("includes/handlers/includedFiles.php");
    if(isset($_GET['id'])){
        $albumId = $_GET['id'];
    }
    else {
        header("Location:index.php");
    }

    $album = new Album($conn, $albumId);
    $artist = $album->getArtist();
    // echo $album->getAlbumTitle() . "<br>";
    // echo $artist->getName();

?>

<div class="entityInfo">
    <div class="leftSection">
        <img src="<?php echo $album->getAlbumPicturePath();?>" alt="">
    </div>
    <div class="rightSection">
        <h2> <?php echo $album->getAlbumTitle();?></h2>
        <p> By  <?php echo $artist->getName();?></p>
        <p><?php echo $album->getNumberSongs();?> </p>
    </div>
</div>

<div class="tracklistContainer">
    <ul class="tracklist">
        <?php
            $songIdArray = $album->getSongsId();
            $i = 1;
            foreach($songIdArray as $songId){
                $albumSong = new Song($conn,$songId);
                $albumArtist = $albumSong->getArtist();

                echo "<li class='tracklistRow'>
                        <div class='trackCount'>
                            <img class='play' src='img/bar-icons/play-white.png' onClick='setTrack(" . $albumSong->getId() . ",tempPlaylist,true)'>
                            <span class='trackNumber'>$i</span>
                        </div>

                        <div class='trackInfo'>
                            <span class='trackName'>" . $albumSong->getTitle() ." </span>
                            <span class='artistName'>" . $albumArtist->getName() ." </span>
                        </div>

                        <div class='trackOptions'>
                            <input type='hidden' class='songId' value='" . $albumSong->getId() .  "'>
                            <img class='optionsButton' src='img/bar-icons/more.png' onClick='showOptionMenu(this)'>
                        </div>
                        <div class='trackDuration'>
                            <span class='duration'>" . $albumSong->getDuration() . "</span>
                        </div>
                    </li>";
                $i++;
            }
        ?>
        <script>
            var tempSongIds = '<?php echo json_encode($songIdArray); ?>';
            tempPlaylist = JSON.parse(tempSongIds);
            // console.log(tempPlaylist);
        </script>
    </ul>
</div>

<nav class="optionMenu">
            <input type="hidden" class="songId">
            <?php echo Playlist::getPlaylistDropdown($conn, $userLoggedIn->getUsername());?>
</nav>
